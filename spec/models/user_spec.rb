require 'rails_helper'

RSpec.describe User, type: :model do
  subject { build(:user) }

  describe 'factory' do
    it { is_expected.to be_valid }
  end

  describe 'associations' do
    it { should have_one(:profile) }
    it { should have_many(:posts) }
    it { should have_many(:comments) }
  end

  describe 'validations' do
    it { should validate_presence_of(:email) }
    it { should validate_uniqueness_of(:email) }

    it { should validate_presence_of(:password) }
    it { should validate_length_of(:password).is_at_least(10) }
  end
end
