FactoryBot.define do
  factory :comment do
    content  { Faker::Movies::Lebowski.quote }

    user
    post
  end
end
