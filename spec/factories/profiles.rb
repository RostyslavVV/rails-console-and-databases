FactoryBot.define do
  factory :profile do
    bio        { Faker::Lorem.sentence }
    first_name { Faker::Name.first_name }
    last_name  { Faker::Name.last_name }
    age        { Faker::Number.between(from: 18, to: 100) }

    user
  end
end
