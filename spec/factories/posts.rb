FactoryBot.define do
  factory :post do
    title    { "#{Faker::TvShows::RickAndMorty.character} at #{Faker::TvShows::RickAndMorty.location}" }
    content  { Faker::TvShows::RickAndMorty.quote }

    user
  end
end
