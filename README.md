# Rails Console And Databases Homework

Hey, dear #Pivorak Students! 🎉

In previous homework you had to implement some real-world object interactions. Hope all of you did it successfully ;)

Our goal now is to implement Twitter domain models with logic.

The main goal is clear, right? So let's go forward and review our requirements in detail!

## Requirements:

1. You need to have basic models:
  - `User` that have next attributes:
    - `email`
    - `password`
  - `Profile` that have next attributes:
    - `first_name`
    - `last_name`
    - `age`
    - `bio`
  - `Post` that have next attributes:
    - `title`
    - `content` - should have ability to store huge amounts of text
  - `Comment` that have next attributes:
    - `content` - should have ability to store huge amounts of text

2. You need to have relations between models:
  - One-to-one:
    - `User` and `Profile`
  - One-to-many:
    - `User` and `Post`
    - `User` and `Comment`
    - `Post` and `Comment`

3. You need to have validations for models attributes:
  - `User`:
    - `email` - should be unique
    - `password` - should have length at least of 10 characters
  - `Profile`:
    - `first_name` - should have length between 2 and 20 characters
    - `last_name` - should have length between 2 and 20 characters
    - `age` - should be integer value
    - `bio` - should have length between 5 and 255 characters
  - `Post`:
    - `title` - should be unique
    - `content` - should have length at least of 15 characters
  - `Comment`:
    - `content` - should have length at least of 15 characters


**Note:** name of attributes should be exactly the same as it is in description.

## Requirements for 1 point

You did everything from mandatory section and following things:

1. Unskip test in spec/models/scopes_spec.rb
2. Add scopes to models based on tests and pass them

## Requirements for 2 points

You did everything from section 'Requirements for 1 point' and also:

1. Implement `Tag` model for storing tags of posts
2. Unskip test in spec/models/tag_spec.rb and pass them


## Assesment criterias:

**-1** : GitlabCI is red or home task was not submitted before 14.06 (Sunday), 11:00 AM (Kyiv time).

**0** : GitlabCI is green

**1** : GitlabCI is green _and_ you implemented ALL requirements for 1 point

**2** : GitlabCI is green _and_ you implemented ALL requirements for 1 point and also ALL requirements (or most of them) for 2 points

## Methods and information that might be helpful while doing hometask

0. Skill of reading specs output
1. `rails generate` and how to deal with it
2. Many to many relations in rails
3. Scopes in Rails models
4. ActiveRecord validations

## Good news:

Number of tries is not limited within given period of time: 07.07 (after lecture ends) till 14.07 (Wednesday), 09:00 PM (Kyiv time). Don't be shy!

To run tests locally:

`bundle install`

`bundle exec rspec`

To run rubocop locally:

`rubocop`
